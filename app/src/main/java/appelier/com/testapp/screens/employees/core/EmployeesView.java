package appelier.com.testapp.screens.employees.core;

import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import java.util.List;

import appelier.com.testapp.R;
import appelier.com.testapp.models.Employee;
import appelier.com.testapp.models.Speciality;
import appelier.com.testapp.screens.employees.EmployeesListActivity;
import appelier.com.testapp.screens.employees.list.EmployeesAdapter;
import butterknife.BindView;
import butterknife.ButterKnife;
import rx.Observable;

/**
 * Created by djaa on 07.12.17.
 */

public class EmployeesView {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.activity_list_recyclerview)
    RecyclerView list;

    private View view;

    private EmployeesAdapter adapter;

    public EmployeesView(EmployeesListActivity context, Speciality speciality) {
        FrameLayout parent = new FrameLayout(context);
        parent.setLayoutParams(new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        view = LayoutInflater.from(context).inflate(R.layout.activity_list, parent, true);
        ButterKnife.bind(this, view);

        context.setSupportActionBar(toolbar);
        context.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        context.getSupportActionBar().setTitle(speciality.getName());

        adapter = new EmployeesAdapter();

        list.setAdapter(adapter);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(context);

        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(list.getContext(),
                mLayoutManager.getOrientation());
        list.addItemDecoration(dividerItemDecoration);

        list.setLayoutManager(mLayoutManager);
    }

    Observable<Integer> itemClicks()
    {
        return adapter.observeClicks();
    }

    public View view() {
        return view;
    }

    void updateAdapter(List<Employee> employees)
    {
        adapter.updateAdapter(employees);
    }

}
