package appelier.com.testapp.screens.specialities.dagger;

import appelier.com.testapp.application.builder.AppComponent;
import appelier.com.testapp.screens.specialities.SpecialitiesListActivity;
import dagger.Component;

/**
 * Created by djaa on 07.12.17.
 */

@SpecialitiesScope
@Component(dependencies = {AppComponent.class} , modules = {SpecialitiesModule.class})
public interface SpecialitiesComponent {

    void inject (SpecialitiesListActivity specialitiesListActivity);
}

