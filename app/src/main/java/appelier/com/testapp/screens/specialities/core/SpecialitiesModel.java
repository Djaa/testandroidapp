package appelier.com.testapp.screens.specialities.core;

import appelier.com.testapp.data.EmployeesDataProvider;
import appelier.com.testapp.models.Specialities;
import appelier.com.testapp.models.Speciality;
import appelier.com.testapp.screens.specialities.SpecialitiesListActivity;
import appelier.com.testapp.utils.NetworkUtils;
import rx.Observable;

/**
 * Created by djaa on 07.12.17.
 */

public class SpecialitiesModel {

    private SpecialitiesListActivity context;
    private EmployeesDataProvider dataProvider;

    public SpecialitiesModel(SpecialitiesListActivity context, EmployeesDataProvider dataProvider) {
        this.dataProvider = dataProvider;
        this.context = context;
    }


    Observable<Specialities> provideListSpecialities() {
        return dataProvider.getSpecialities();
    }

    Observable<Boolean> isNetworkAvailable() {
        return NetworkUtils.isNetworkAvailableObservable(context);
    }


    void gotoSpecialityDetailsActivity(Speciality speciality) {
        context.goToSpecialityEmployeesActivity(speciality);
    }

}
