package appelier.com.testapp.screens.employees;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import javax.inject.Inject;

import appelier.com.testapp.application.AppController;
import appelier.com.testapp.models.Employee;
import appelier.com.testapp.models.Speciality;
import appelier.com.testapp.screens.details.EmployeeDetailsActivity;
import appelier.com.testapp.screens.employees.core.EmployeesPresenter;
import appelier.com.testapp.screens.employees.core.EmployeesView;
import appelier.com.testapp.screens.employees.dagger.DaggerEmployeesComponent;
import appelier.com.testapp.screens.employees.dagger.EmployeesModule;

/**
 * Created by djaa on 07.12.17.
 */

public class EmployeesListActivity extends AppCompatActivity {

    public static final String KEY_SPECIALITY_ID = "speciality_id";

    @Inject
    EmployeesView view;
    @Inject
    EmployeesPresenter presenter;

    @Override
    public void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Speciality speciality = (Speciality) getIntent().getSerializableExtra(KEY_SPECIALITY_ID);

        DaggerEmployeesComponent.builder()
                .appComponent(AppController.getNetComponent()).employeesModule(new EmployeesModule(this, speciality)).build().inject(this);
        setContentView(view.view());
        presenter.onCreate();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.onDestroy();
    }


    public void gotoEmployeeDetailsActivity(Employee employee) {
        Intent in = new Intent(this, EmployeeDetailsActivity.class);
        in.putExtra(EmployeeDetailsActivity.KEY_EMPLOYEE, employee);
        startActivity(in);
    }
}
