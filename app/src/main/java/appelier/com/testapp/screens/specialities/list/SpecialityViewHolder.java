package appelier.com.testapp.screens.specialities.list;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import appelier.com.testapp.R;
import appelier.com.testapp.models.Speciality;
import butterknife.BindView;
import butterknife.ButterKnife;
import rx.subjects.PublishSubject;

/**
 * Created by djaa on 07.12.17.
 */

class SpecialityViewHolder extends RecyclerView.ViewHolder {

    private View view;

    @BindView(R.id.speciality_name)
    TextView specialityName;


    SpecialityViewHolder(View itemView, PublishSubject<Integer> itemClicks) {
        super(itemView);
        this.view = itemView;
        ButterKnife.bind(this, view);
        view.setOnClickListener(v -> itemClicks.onNext(getAdapterPosition()));
    }

    void bind(Speciality speciality) {
        specialityName.setText(speciality.getName());
    }

}
