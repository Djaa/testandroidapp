package appelier.com.testapp.screens.specialities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;

import java.io.Serializable;

import javax.inject.Inject;

import appelier.com.testapp.application.AppController;
import appelier.com.testapp.models.Speciality;
import appelier.com.testapp.screens.employees.EmployeesListActivity;
import appelier.com.testapp.screens.specialities.core.SpecialitiesPresenter;
import appelier.com.testapp.screens.specialities.core.SpecialitiesView;
import appelier.com.testapp.screens.specialities.dagger.DaggerSpecialitiesComponent;
import appelier.com.testapp.screens.specialities.dagger.SpecialitiesModule;

public class SpecialitiesListActivity extends AppCompatActivity {

    @Inject
    SpecialitiesView view;
    @Inject
    SpecialitiesPresenter presenter;


    @Override
    public void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        DaggerSpecialitiesComponent.builder()
                .appComponent(AppController.getNetComponent()).specialitiesModule(new SpecialitiesModule(this)).build().inject(this);
        setContentView(view.view());
        presenter.onCreate();


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.onDestroy();
    }

    public void goToSpecialityEmployeesActivity(Speciality speciality) {
        Intent in = new Intent(this, EmployeesListActivity.class);
        in.putExtra(EmployeesListActivity.KEY_SPECIALITY_ID, (Serializable) speciality);
        startActivity(in);
    }

}
