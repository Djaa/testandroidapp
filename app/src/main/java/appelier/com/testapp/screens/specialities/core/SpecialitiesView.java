package appelier.com.testapp.screens.specialities.core;

import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import java.util.List;

import appelier.com.testapp.R;
import appelier.com.testapp.models.Speciality;
import appelier.com.testapp.screens.specialities.SpecialitiesListActivity;
import appelier.com.testapp.screens.specialities.list.SpecialitiesAdapter;
import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import rx.Observable;

/**
 * Created by djaa on 06.12.17.
 */

public class SpecialitiesView {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.activity_list_recyclerview)
    RecyclerView list;

    @BindString(R.string.specialities_title_label)
    String toolbarTitle;

    private View view;

    private SpecialitiesAdapter adapter;

    public SpecialitiesView(SpecialitiesListActivity context) {
        FrameLayout parent = new FrameLayout(context);
        parent.setLayoutParams(new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        view = LayoutInflater.from(context).inflate(R.layout.activity_list, parent, true);
        ButterKnife.bind(this, view);

        context.setSupportActionBar(toolbar);
        context.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        context.getSupportActionBar().setTitle(toolbarTitle);

        adapter = new SpecialitiesAdapter();

        list.setAdapter(adapter);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(context);

        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(list.getContext(),
                mLayoutManager.getOrientation());
        list.addItemDecoration(dividerItemDecoration);

        list.setLayoutManager(mLayoutManager);
    }

    Observable<Integer> itemClicks()
    {
        return adapter.observeClicks();
    }

    public View view() {
        return view;
    }

    void swapAdapter(List<Speciality> specialities)
    {
        adapter.updateAdapter(specialities);
    }

}
