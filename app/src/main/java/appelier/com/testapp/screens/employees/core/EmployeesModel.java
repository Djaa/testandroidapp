package appelier.com.testapp.screens.employees.core;

import appelier.com.testapp.data.EmployeesDataProvider;
import appelier.com.testapp.models.Employee;
import appelier.com.testapp.models.Employees;
import appelier.com.testapp.screens.employees.EmployeesListActivity;
import appelier.com.testapp.utils.NetworkUtils;
import rx.Observable;

/**
 * Created by djaa on 07.12.17.
 */

public class EmployeesModel {

    private EmployeesListActivity context;
    private EmployeesDataProvider dataProvider;

    public EmployeesModel(EmployeesListActivity context, EmployeesDataProvider dataProvider) {
        this.dataProvider = dataProvider;
        this.context = context;
    }

    Observable<Employees> provideListEmployees(int specialityId) {
        return dataProvider.getEmployeesBySpeciality(specialityId);
    }

    Observable<Boolean> isNetworkAvailable() {
        return NetworkUtils.isNetworkAvailableObservable(context);
    }


    void gotoEmployeeDetailsActivity(Employee employee) {
        context.gotoEmployeeDetailsActivity(employee);
    }

}
