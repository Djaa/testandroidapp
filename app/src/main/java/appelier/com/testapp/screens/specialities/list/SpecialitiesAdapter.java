package appelier.com.testapp.screens.specialities.list;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import appelier.com.testapp.R;
import appelier.com.testapp.models.Speciality;
import rx.Observable;
import rx.subjects.PublishSubject;

/**
 * Created by djaa on 07.12.17.
 */

public class SpecialitiesAdapter extends RecyclerView.Adapter<SpecialityViewHolder> {

    private final PublishSubject<Integer> itemClicks = PublishSubject.create();
    private List<Speciality> specialities = new LinkedList<>();

    @Override
    public SpecialityViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.speciality_item, parent, false);
        return new SpecialityViewHolder(view, itemClicks);
    }

    @Override
    public void onBindViewHolder(SpecialityViewHolder holder, int position) {

        Speciality speciality = specialities.get(position);
        holder.bind(speciality);
    }

    @Override
    public int getItemCount() {
        return specialities.size();
    }

    public void updateAdapter(List<Speciality> specialities) {

        this.specialities.clear();
        this.specialities.addAll(specialities);
        notifyDataSetChanged();
    }

    public Observable<Integer> observeClicks() {
        return itemClicks;
    }
}
