package appelier.com.testapp.screens.details.dagger;

import appelier.com.testapp.models.Employee;
import appelier.com.testapp.screens.details.EmployeeDetailsActivity;
import appelier.com.testapp.screens.details.core.EmployeeDetailsView;
import dagger.Module;
import dagger.Provides;

/**
 * Created by djaa on 07.12.17.
 */

@Module
public class EmployeeDetailsModule {
    EmployeeDetailsActivity detailsContext;
    Employee employee;

    public EmployeeDetailsModule(EmployeeDetailsActivity context, Employee employee) {
        this.detailsContext = context;
        this.employee = employee;
    }

    @Provides
    EmployeeDetailsView provideView() {
        return  new EmployeeDetailsView(detailsContext, employee);
    }
}


