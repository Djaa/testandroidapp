package appelier.com.testapp.screens.employees.dagger;

import appelier.com.testapp.data.EmployeesDataProvider;
import appelier.com.testapp.models.Speciality;
import appelier.com.testapp.screens.employees.EmployeesListActivity;
import appelier.com.testapp.screens.employees.core.EmployeesModel;
import appelier.com.testapp.screens.employees.core.EmployeesPresenter;
import appelier.com.testapp.screens.employees.core.EmployeesView;
import appelier.com.testapp.utils.rx.RxSchedulers;
import dagger.Module;
import dagger.Provides;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by djaa on 07.12.17.
 */

@Module
public class EmployeesModule {

    private EmployeesListActivity employeesListContext;
    private Speciality speciality;

    public EmployeesModule(EmployeesListActivity context, Speciality specialityId) {
        this.employeesListContext = context;
        this.speciality = specialityId;
    }



    @EmployeesScope
    @Provides
    EmployeesView provideView() {
        return new EmployeesView(employeesListContext, speciality);
    }

    @EmployeesScope
    @Provides
    EmployeesPresenter providePresenter(RxSchedulers schedulers, EmployeesView view, EmployeesModel model) {
        CompositeSubscription subscriptions = new CompositeSubscription();
        return new EmployeesPresenter(schedulers, model, view, subscriptions, speciality);
    }



    @EmployeesScope
    @Provides
    EmployeesListActivity provideContext() {
        return employeesListContext;
    }

    @EmployeesScope
    @Provides
    EmployeesModel provideModel(EmployeesDataProvider dataProvider) {
        return new EmployeesModel(employeesListContext, dataProvider);
    }
}
