package appelier.com.testapp.screens.details.core;

import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import appelier.com.testapp.R;
import appelier.com.testapp.models.Employee;
import appelier.com.testapp.screens.details.EmployeeDetailsActivity;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by djaa on 07.12.17.
 */

public class EmployeeDetailsView {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.employee_first_name)
    TextView employeeFirstName;

    @BindView(R.id.employee_last_name)
    TextView employeeLastName;

    @BindView(R.id.employee_age)
    TextView employeeAge;

    @BindView(R.id.employee_avatar)
    ImageView employeeAvatar;

    @BindView(R.id.employee_details_container)
    View detailsContainer;

    @BindView(R.id.employee_birthday)
    TextView employeeBirthday;

    @BindView(R.id.employee_speciality)
    TextView employeeSpeciality;


    private View view;

    public EmployeeDetailsView(EmployeeDetailsActivity activity, Employee employee) {
        FrameLayout layout = new FrameLayout(activity);
        layout.setLayoutParams(new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

        view = LayoutInflater.from(activity).inflate(R.layout.activity_employee_details,layout,true);
        ButterKnife.bind(this,view);

        activity.setSupportActionBar(toolbar);
        activity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        activity.getSupportActionBar().setTitle(employee.getFirst_name());

        Glide.with(view.getContext()).load(employee.getAvatar_url()).placeholder(R.drawable.ic_launcher_background).into(employeeAvatar);

        detailsContainer.setVisibility(View.VISIBLE);
        employeeFirstName.setText(employee.getFirst_name());
        employeeLastName.setText(employee.getLast_name());
        employeeAge.setText(employee.getAge() == -1 ? "..." : String.valueOf(employee.getAge()));
        employeeBirthday.setText(employee.getBirthday());

        String employeeSpecialities = employee.getSpecialties().toString().replace("[", "").replace("]", "");
        employeeSpeciality.setText(employeeSpecialities);
    }

    public View view(){
        return view;
    }
}
