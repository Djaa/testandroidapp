package appelier.com.testapp.screens.specialities.core;

import android.support.design.widget.BaseTransientBottomBar;
import android.support.design.widget.Snackbar;

import java.util.ArrayList;
import java.util.List;

import appelier.com.testapp.models.Speciality;
import appelier.com.testapp.utils.rx.RxSchedulers;
import rx.Subscription;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by djaa on 07.12.17.
 */

public class SpecialitiesPresenter {

    private SpecialitiesView view;
    private SpecialitiesModel model;
    private RxSchedulers rxSchedulers;
    private CompositeSubscription subscriptions;
    private List<Speciality> specialities = new ArrayList<>();

    public SpecialitiesPresenter(RxSchedulers schedulers, SpecialitiesModel model, SpecialitiesView view, CompositeSubscription sub) {
        this.rxSchedulers = schedulers;
        this.view = view;
        this.model = model;
        this.subscriptions = sub;
    }

    public void onCreate() {

        subscriptions.add(getSpecialitiesList());
        subscriptions.add(respondToClick());
    }

    public void onDestroy() {
        subscriptions.clear();
    }


    private Subscription respondToClick() {

        return view.itemClicks().subscribe(integer -> model.gotoSpecialityDetailsActivity(specialities.get(integer)));
    }


    private Subscription getSpecialitiesList() {

        return model.isNetworkAvailable()
                .doOnNext(networkAvailable -> {
                    if (!networkAvailable) {
                        Snackbar.make(view.view(), "Error, no connection.", BaseTransientBottomBar.LENGTH_LONG).show();
                    }
                })
                .filter(isNetworkAvailable -> true)
                .flatMap(isAvailable -> model.provideListSpecialities())
                .subscribeOn(rxSchedulers.internet())
                .observeOn(rxSchedulers.androidThread()).subscribe(specialities -> {
                    view.swapAdapter(specialities.getElements());
                    this.specialities = specialities.getElements();

                }, throwable -> {
                    throwable.printStackTrace();
                            Snackbar.make(view.view(), "Error, please try again.", BaseTransientBottomBar.LENGTH_LONG).show();
                }
        );

    }

}
