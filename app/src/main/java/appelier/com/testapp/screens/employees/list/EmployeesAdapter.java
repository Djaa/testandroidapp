package appelier.com.testapp.screens.employees.list;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.LinkedList;
import java.util.List;

import appelier.com.testapp.R;
import appelier.com.testapp.models.Employee;
import rx.Observable;
import rx.subjects.PublishSubject;

/**
 * Created by djaa on 07.12.17.
 */

public class EmployeesAdapter extends RecyclerView.Adapter<EmployeesViewHolder> {

    private final PublishSubject<Integer> itemClicks = PublishSubject.create();
    private List<Employee> employees = new LinkedList<>();

    @Override
    public EmployeesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.employee_item, parent, false);
        return new EmployeesViewHolder(view, itemClicks);
    }

    @Override
    public void onBindViewHolder(EmployeesViewHolder holder, int position) {

        Employee employee = employees.get(position);
        holder.bind(employee);
    }

    @Override
    public int getItemCount() {
        return employees.size();
    }

    public void updateAdapter(List<Employee> employees) {

        this.employees.clear();
        this.employees.addAll(employees);
        notifyDataSetChanged();
    }

    public Observable<Integer> observeClicks() {
        return itemClicks;
    }
}
