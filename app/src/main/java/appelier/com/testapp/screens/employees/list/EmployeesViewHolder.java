package appelier.com.testapp.screens.employees.list;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import appelier.com.testapp.R;
import appelier.com.testapp.models.Employee;
import butterknife.BindView;
import butterknife.ButterKnife;
import rx.subjects.PublishSubject;

/**
 * Created by djaa on 07.12.17.
 */

class EmployeesViewHolder extends RecyclerView.ViewHolder {

    private View view;

    @BindView(R.id.employee_first_name)
    TextView employeeFirstName;
    @BindView(R.id.employee_last_name)
    TextView employeeLastName;
    @BindView(R.id.employee_age)
    TextView employeeAge;
    @BindView(R.id.employee_avatar)
    ImageView employeeAvatar;


    EmployeesViewHolder(View itemView, PublishSubject<Integer> itemClicks) {
        super(itemView);
        this.view = itemView;
        ButterKnife.bind(this, view);
        view.setOnClickListener(v -> itemClicks.onNext(getAdapterPosition()));
    }

    void bind(Employee employee) {
        Glide.with(view.getContext()).load(employee.getAvatar_url()).placeholder(R.drawable.ic_launcher_background).into(employeeAvatar);

        employeeFirstName.setText(employee.getFirst_name());
        employeeLastName.setText(employee.getLast_name());
        employeeAge.setText(employee.getAge() == -1 ? "..." : String.valueOf(employee.getAge()));
    }

}
