package appelier.com.testapp.screens.employees.core;

import android.support.design.widget.BaseTransientBottomBar;
import android.support.design.widget.Snackbar;

import java.util.ArrayList;
import java.util.List;

import appelier.com.testapp.models.Employee;
import appelier.com.testapp.models.Speciality;
import appelier.com.testapp.utils.rx.RxSchedulers;
import rx.Subscription;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by djaa on 07.12.17.
 */

public class EmployeesPresenter {

    private EmployeesView view;
    private EmployeesModel model;
    private RxSchedulers rxSchedulers;
    private CompositeSubscription subscriptions;
    private Speciality speciality;
    private List<Employee> employees = new ArrayList<>();

    public EmployeesPresenter(RxSchedulers schedulers, EmployeesModel model, EmployeesView view, CompositeSubscription sub, Speciality speciality) {
        this.rxSchedulers = schedulers;
        this.view = view;
        this.model = model;
        this.subscriptions = sub;
        this.speciality = speciality;
    }

    public void onCreate() {
        subscriptions.add(getSpecialitiesList());
        subscriptions.add(respondToClick());
    }

    public void onDestroy() {
        subscriptions.clear();
    }


    private Subscription respondToClick() {

        return view.itemClicks().subscribe(integer -> model.gotoEmployeeDetailsActivity(employees.get(integer)));
    }


    private Subscription getSpecialitiesList() {
        return model.provideListEmployees(speciality.getSpecialty_id())
                .subscribeOn(rxSchedulers.io())
                .observeOn(rxSchedulers.androidThread())
                .subscribe(employees -> {
                    view.updateAdapter(employees.getEmployees());
                    this.employees = employees.getEmployees();

                }, throwable -> {
                    throwable.printStackTrace();
                    Snackbar.make(view.view(), "Error, please try again.", BaseTransientBottomBar.LENGTH_LONG).show();
                }
        );

    }


}
