package appelier.com.testapp.screens.details.dagger;

import appelier.com.testapp.screens.details.EmployeeDetailsActivity;
import dagger.Component;

/**
 * Created by djaa on 07.12.17.
 */

@Component(modules = {EmployeeDetailsModule.class})
public interface EmployeeDetailsComponent {
    void inject(EmployeeDetailsActivity context);
}
