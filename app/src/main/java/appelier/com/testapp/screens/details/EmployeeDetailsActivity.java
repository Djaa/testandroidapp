package appelier.com.testapp.screens.details;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import javax.inject.Inject;

import appelier.com.testapp.models.Employee;
import appelier.com.testapp.screens.details.core.EmployeeDetailsView;
import appelier.com.testapp.screens.details.dagger.DaggerEmployeeDetailsComponent;
import appelier.com.testapp.screens.details.dagger.EmployeeDetailsModule;

/**
 * Created by djaa on 07.12.17.
 */

public class EmployeeDetailsActivity extends AppCompatActivity {

    public static final String KEY_EMPLOYEE = "speciality_id";

    @Inject
    EmployeeDetailsView view;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Employee employee = (Employee) getIntent().getSerializableExtra(KEY_EMPLOYEE);

        DaggerEmployeeDetailsComponent.builder().employeeDetailsModule(new EmployeeDetailsModule(this, employee)).build().inject(this);

        setContentView(view.view());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

}
