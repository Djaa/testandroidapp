package appelier.com.testapp.screens.employees.dagger;

import appelier.com.testapp.application.builder.AppComponent;
import appelier.com.testapp.screens.employees.EmployeesListActivity;
import dagger.Component;

/**
 * Created by djaa on 07.12.17.
 */

@EmployeesScope
@Component(dependencies = {AppComponent.class} , modules = {EmployeesModule.class})
public interface EmployeesComponent {

    void inject(EmployeesListActivity employeesListActivity);
}

