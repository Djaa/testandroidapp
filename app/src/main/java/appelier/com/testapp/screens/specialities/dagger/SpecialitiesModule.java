package appelier.com.testapp.screens.specialities.dagger;

import appelier.com.testapp.data.EmployeesDataProvider;
import appelier.com.testapp.screens.specialities.SpecialitiesListActivity;
import appelier.com.testapp.screens.specialities.core.SpecialitiesModel;
import appelier.com.testapp.screens.specialities.core.SpecialitiesPresenter;
import appelier.com.testapp.screens.specialities.core.SpecialitiesView;
import appelier.com.testapp.utils.rx.RxSchedulers;
import dagger.Module;
import dagger.Provides;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by djaa on 07.12.17.
 */

@Module
public class SpecialitiesModule {

    SpecialitiesListActivity specialitiesListContext;

    public SpecialitiesModule(SpecialitiesListActivity context) {
        this.specialitiesListContext = context;
    }



    @SpecialitiesScope
    @Provides
    SpecialitiesView provideView() {
        return new SpecialitiesView(specialitiesListContext);
    }

    @SpecialitiesScope
    @Provides
    SpecialitiesPresenter providePresenter(RxSchedulers schedulers, SpecialitiesView view, SpecialitiesModel model) {
        CompositeSubscription subscriptions = new CompositeSubscription();
        return new SpecialitiesPresenter(schedulers, model, view, subscriptions);
    }



    @SpecialitiesScope
    @Provides
    SpecialitiesListActivity provideContext() {
        return specialitiesListContext;
    }

    @SpecialitiesScope
    @Provides
    SpecialitiesModel provideModel(EmployeesDataProvider dataProvider) {
        return new SpecialitiesModel(specialitiesListContext, dataProvider);
    }
}
