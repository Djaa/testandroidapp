package appelier.com.testapp.data;

import appelier.com.testapp.models.Employees;
import appelier.com.testapp.models.Specialities;
import rx.Observable;

/**
 * Created by djaa on 07.12.17.
 */

public interface EmployeesDataProvider {

    Observable<Employees> getEmployeesBySpeciality(int specialityId);
    Observable<Specialities> getSpecialities();

}
