package appelier.com.testapp.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by djaa on 07.12.17.
 */

public class ApplicationDBHelper extends SQLiteOpenHelper {
    private static final int DB_VERSION = 1;
    private static final String DB_NAME = "testdb.db";
    private SQLiteDatabase sqLiteDatabase;

    public ApplicationDBHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    public synchronized SQLiteDatabase getSingleDatabase(){
        if (this.sqLiteDatabase == null){
            this.sqLiteDatabase = this.getWritableDatabase();
        }

        return this.sqLiteDatabase;
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        Log.d("DB", "onCreate() : creating COLLECTIONS TABLE");
        sqLiteDatabase.execSQL(EmployeesService.DDL_EMPLOYEES_TABLE);
        sqLiteDatabase.execSQL(EmployeesService.DDL_SPECIALITIES_TABLE);
        sqLiteDatabase.execSQL(EmployeesService.DDL_EMPLOYEES_SPECIALITIES_TABLE);

        Log.d("DB", "onCreate() : creating COLLECTIONS TABLE completed successfully");
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        // TODO: implement this.
    }

}
