package appelier.com.testapp.data;

import android.content.ContentValues;
import android.database.Cursor;

import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.joda.time.Years;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

import javax.inject.Singleton;

import appelier.com.testapp.api.EmployeesApi;
import appelier.com.testapp.models.Employee;
import appelier.com.testapp.models.Employees;
import appelier.com.testapp.models.Specialities;
import appelier.com.testapp.models.Speciality;
import appelier.com.testapp.utils.DbUtils;
import rx.Observable;
import rx.Subscriber;
import rx.schedulers.Schedulers;

/**
 * Created by djaa on 07.12.17.
 */

@Singleton
public class EmployeesService implements EmployeesDataProvider {

    public static final String DDL_EMPLOYEES_TABLE = "CREATE TABLE EMPLOYEES(" +
            " EMPLOYEE_ID CHAR(40) PRIMARY KEY," +
            " FIRST_NAME CHAR(50) NOT NULL," +
            " LAST_NAME CHAR(50) NOT NULL," +
            " BIRTHDAY CHAR(12)," +
            " AVATAR_URL CHAR(100));";

    public static final String DDL_EMPLOYEES_SPECIALITIES_TABLE = "CREATE TABLE EMPLOYEES_SPECIALITIES(" +
            " EMPLOYEE_ID REFERENCES EMPLOYEES(EMPLOYEE_ID)," +
            " KEY_SPECIALITY_ID REFERENCES SPECIALITIES(KEY_SPECIALITY_ID));";

    public static final String DDL_SPECIALITIES_TABLE = "CREATE TABLE SPECIALITIES(" +
            " KEY_SPECIALITY_ID INTEGER PRIMARY KEY," +
            " NAME CHAR(50) NOT NULL);";


    private EmployeesApi api;
    private ApplicationDBHelper dbHelper;

    public EmployeesService(EmployeesApi api, ApplicationDBHelper dbHelper) {
        this.api = api;
        this.dbHelper = dbHelper;
    }

    @Override
    public Observable<Employees> getEmployeesBySpeciality(int specialityId) {
        return Observable.unsafeCreate(subscriber -> {

            Cursor cursor = null;
            List<Employee> employeeList = new LinkedList<>();

            try {
                cursor = dbHelper.getSingleDatabase().rawQuery("SELECT * FROM EMPLOYEES e " +
                        "JOIN EMPLOYEES_SPECIALITIES es on e.EMPLOYEE_ID = es.EMPLOYEE_ID WHERE es.KEY_SPECIALITY_ID = " + specialityId + ";", null);

                cursor.moveToFirst();

                while (!cursor.isAfterLast()) {
                    Employee employee = new Employee();

                    employee.setId(cursor.getString(cursor.getColumnIndex("EMPLOYEE_ID")));
                    employee.setFirst_name(cursor.getString(cursor.getColumnIndex("FIRST_NAME")));
                    employee.setLast_name(cursor.getString(cursor.getColumnIndex("LAST_NAME")));
                    employee.setBirthday(cursor.getString(cursor.getColumnIndex("BIRTHDAY")));
                    employee.setAvatar_url(cursor.getString(cursor.getColumnIndex("AVATAR_URL")));

                    addSpecialities(employee);
                    countAge(employee);

                    employeeList.add(employee);
                    cursor.moveToNext();
                }

                Employees employees = new Employees();
                employees.setEmployees(employeeList);

                subscriber.onNext(employees);
                subscriber.onCompleted();
            } catch (Exception e) {
                e.printStackTrace();
                subscriber.onError(e);
            } finally {
                DbUtils.closeCursor(cursor);
            }

        });
    }

    private void countAge(Employee employee) {
        if (employee.getBirthday() == null || employee.getBirthday().isEmpty()) {
            employee.setAge(-1);
            return;
        }

        DateTimeFormatter dateFormat = DateTimeFormat.forPattern("dd.MM.yyyy");
        DateTime dateTime = dateFormat.parseDateTime(employee.getBirthday());

        Years years = Years.yearsBetween(dateTime.toLocalDate(), new LocalDate());
        employee.setAge(years.getYears());
    }

    private void addSpecialities(Employee employee) {
        Cursor cursor = null;
        try {
            cursor = dbHelper.getSingleDatabase().rawQuery("SELECT * FROM SPECIALITIES s " +
                    "JOIN EMPLOYEES_SPECIALITIES es on s.KEY_SPECIALITY_ID = es.KEY_SPECIALITY_ID WHERE es.EMPLOYEE_ID = '" + employee.getId() + "';", null);

            cursor.moveToFirst();

            while (!cursor.isAfterLast()) {
                Speciality speciality = new Speciality();

                speciality.setSpecialty_id(cursor.getInt(cursor.getColumnIndex("KEY_SPECIALITY_ID")));
                speciality.setName(cursor.getString(cursor.getColumnIndex("NAME")));

                employee.getSpecialties().add(speciality);
                cursor.moveToNext();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            DbUtils.closeCursor(cursor);
        }
    }

    @Override
    public Observable<Specialities> getSpecialities() {
        return Observable.unsafeCreate(subscriber -> {

            Cursor cursor = null;
            List<Speciality> specialityList = new LinkedList<>();
            try {
                cursor = dbHelper.getSingleDatabase().rawQuery("SELECT * FROM SPECIALITIES;", null);
                cursor.moveToFirst();

                if (cursor.isAfterLast()) {
                    // DB is empty
                    retrieveDataFromApi(subscriber);
                    return;
                }

                while (!cursor.isAfterLast()) {
                    Speciality speciality = new Speciality();
                    speciality.setSpecialty_id(cursor.getInt(cursor.getColumnIndex("KEY_SPECIALITY_ID")));
                    speciality.setName(cursor.getString(cursor.getColumnIndex("NAME")));
                    specialityList.add(speciality);

                    cursor.moveToNext();
                }

                Specialities specialities = new Specialities();
                specialities.setElements(specialityList);

                subscriber.onNext(specialities);
                subscriber.onCompleted();
            } catch (Exception e) {
                e.printStackTrace();

                subscriber.onError(e);
            } finally {
                DbUtils.closeCursor(cursor);
            }
        });
    }

    private void retrieveDataFromApi(Subscriber<? super Specialities> subscriber) {
        api.getEmployeesData()
                .observeOn(Schedulers.io())
                .doOnNext(employees -> {

                    List<Speciality> specialityList = new LinkedList<>();
                    for (Employee employee : employees.getEmployees()) {
                        employee.setId(UUID.randomUUID().toString());
                        normalizeEmployeeData(employee);

                        for (Speciality speciality : employee.getSpecialties()) {
                            if (!specialityList.contains(speciality)) {
                                specialityList.add(speciality);
                            }
                        }
                    }

                    saveSpecialities(specialityList);
                    saveEmployees(employees.getEmployees());
                    saveReferences(employees.getEmployees());

                    Specialities specialities = new Specialities();
                    specialities.setElements(specialityList);

                    subscriber.onNext(specialities);
                })
                .subscribe();
    }

    private void saveSpecialities(List<Speciality> specialityList) {
        for (Speciality speciality : specialityList) {
            ContentValues contentValues = new ContentValues();
            contentValues.put("KEY_SPECIALITY_ID", speciality.getSpecialty_id());
            contentValues.put("NAME", speciality.getName());

            dbHelper.getSingleDatabase().insert("SPECIALITIES", null, contentValues);
        }
    }

    private void saveEmployees(List<Employee> employeeList) {
        for (Employee employee : employeeList) {
            ContentValues contentValues = new ContentValues();

            contentValues.put("EMPLOYEE_ID", employee.getId());
            contentValues.put("FIRST_NAME", employee.getFirst_name());
            contentValues.put("LAST_NAME", employee.getLast_name());
            contentValues.put("BIRTHDAY", employee.getBirthday());
            contentValues.put("AVATAR_URL", employee.getAvatar_url());

            dbHelper.getSingleDatabase().insert("EMPLOYEES", null, contentValues);
        }
    }

    private void saveReferences(List<Employee> employeeList) {
        for (Employee employee : employeeList) {
            for (Speciality speciality : employee.getSpecialties()) {
                ContentValues contentValues = new ContentValues();

                contentValues.put("EMPLOYEE_ID", employee.getId());
                contentValues.put("KEY_SPECIALITY_ID", speciality.getSpecialty_id());

                dbHelper.getSingleDatabase().insert("EMPLOYEES_SPECIALITIES", null, contentValues);
            }
        }
    }

    private void normalizeEmployeeData(Employee employee) {
        employee.setFirst_name(DbUtils.capitalizeFirstLetter(employee.getFirst_name()));
        employee.setLast_name(DbUtils.capitalizeFirstLetter(employee.getLast_name()));

        if (employee.getBirthday() == null || employee.getBirthday().isEmpty()) {
            return;
        }

        String oldDate = employee.getBirthday();

        DateTimeFormatter oldDateFormat = DateTimeFormat.forPattern("yyyy-MM-dd");
        DateTimeFormatter alterOldDateFormat = DateTimeFormat.forPattern("dd-MM-yyyy");
        DateTimeFormatter newDateFormat = DateTimeFormat.forPattern("dd.MM.yyyy");

        try {
            DateTime dateTime = oldDateFormat.parseDateTime(oldDate);
            employee.setBirthday(newDateFormat.print(dateTime));
        } catch (IllegalArgumentException e) {
            try {
                DateTime dateTime = alterOldDateFormat.parseDateTime(oldDate);
                employee.setBirthday(newDateFormat.print(dateTime));
            } catch (IllegalArgumentException e1) {
                // Well, it is more than 2 types of date format coming from API if it happens I'd use regex
                e1.printStackTrace();
            }
        }
    }
}
