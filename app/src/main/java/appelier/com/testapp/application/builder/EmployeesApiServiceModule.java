package appelier.com.testapp.application.builder;

import android.content.Context;
import android.util.Log;

import appelier.com.testapp.api.EmployeesApi;
import appelier.com.testapp.data.ApplicationDBHelper;
import appelier.com.testapp.data.EmployeesDataProvider;
import appelier.com.testapp.data.EmployeesService;
import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by djaa on 07.12.17.
 */

@Module
public class EmployeesApiServiceModule {

    private static final String BASE_URL = "http://gitlab.65apps.com/";
    @AppScope
    @Provides
    EmployeesDataProvider provideApiService(OkHttpClient client, GsonConverterFactory gson, RxJavaCallAdapterFactory rxAdapter, Context context)
    {
        Retrofit retrofit =   new Retrofit.Builder().client(client)
                .baseUrl(BASE_URL).addConverterFactory(gson).
                        addCallAdapterFactory(rxAdapter).build();

        EmployeesApi employeesApi = retrofit.create(EmployeesApi.class);
        ApplicationDBHelper dbHelper = new ApplicationDBHelper(context);

        return new EmployeesService(employeesApi, dbHelper);
    }
}
