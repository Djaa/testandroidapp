package appelier.com.testapp.application.builder;

import appelier.com.testapp.api.EmployeesApi;
import appelier.com.testapp.data.EmployeesDataProvider;
import appelier.com.testapp.data.EmployeesService;
import appelier.com.testapp.utils.rx.RxSchedulers;
import dagger.Component;

/**
 * Created by djaa on 06.12.17.
 */

@AppScope
@Component(modules = {NetworkModule.class, AppContextModule.class, RxModule.class, EmployeesApiServiceModule.class})
public interface AppComponent {

    RxSchedulers rxSchedulers();
    EmployeesDataProvider employeesService();

}
