package appelier.com.testapp.application.builder;

import appelier.com.testapp.utils.rx.AppRxSchedulers;
import appelier.com.testapp.utils.rx.RxSchedulers;
import dagger.Module;
import dagger.Provides;

/**
 * Created by djaa on 07.12.17.
 */

@Module
class RxModule {

    @Provides
    RxSchedulers provideRxSchedulers() {
        return new AppRxSchedulers();
    }
}
