package appelier.com.testapp.application.builder;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 * Created by djaa on 07.12.17.
 */

@Scope
@Retention(RetentionPolicy.CLASS)
@interface AppScope {
}
