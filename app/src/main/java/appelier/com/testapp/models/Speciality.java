package appelier.com.testapp.models;

import com.google.gson.annotations.Expose;

import java.io.Serializable;

/**
 * Created by djaa on 07.12.17.
 */

public class Speciality implements Serializable {

    @Expose
    private int specialty_id;

    @Expose
    private String name;


    public int getSpecialty_id() {
        return specialty_id;
    }

    public void setSpecialty_id(int specialty_id) {
        this.specialty_id = specialty_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object other) {
        if (other == null || !(other instanceof Speciality)) return false;
        return ((Speciality) other).getSpecialty_id() == this.specialty_id;
    }

    @Override
    public String toString() {
        return name;
    }
}
