package appelier.com.testapp.models;

import com.google.gson.annotations.Expose;

import java.util.List;

/**
 * Created by djaa on 07.12.17.
 */

public class Specialities {

    @Expose
    private List<Speciality> elements;

    public List<Speciality> getElements() {
        return elements;
    }

    public void setElements(List<Speciality> elements) {
        this.elements = elements;
    }


}
