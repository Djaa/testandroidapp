package appelier.com.testapp.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by djaa on 07.12.17.
 */

public class Employee implements Serializable {

    private String id;

    private int age;

    @Expose
    @SerializedName("f_name")
    private String first_name;

    @Expose
    @SerializedName("l_name")
    private String last_name;

    @Expose
    @SerializedName("avatr_url")
    private String avatar_url;

    @Expose
    @SerializedName("birthday")
    private String birthday;

    @Expose
    @SerializedName("specialty")
    private List<Speciality> specialties = new LinkedList<>();


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getAvatar_url() {
        return avatar_url;
    }

    public void setAvatar_url(String avatar_url) {
        this.avatar_url = avatar_url;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public List<Speciality> getSpecialties() {
        return specialties;
    }

    public void setSpecialties(List<Speciality> specialties) {
        this.specialties = specialties;
    }
}
