package appelier.com.testapp.api;

import appelier.com.testapp.models.Employees;
import retrofit2.http.GET;
import rx.Observable;

/**
 * Created by djaa on 07.12.17.
 */

public interface EmployeesApi {

    @GET("65gb/static/raw/master/testTask.json")
    Observable<Employees> getEmployeesData();

}
