package appelier.com.testapp.utils;

import android.database.Cursor;
import android.util.Log;

/**
 * Created by djaa on 07.12.17.
 */

public class DbUtils {
    public static void closeCursor(Cursor cursor){
        if (cursor != null){
            try {
                cursor.close();
            } catch (Exception e){
                Log.e("CursorClose", "Ignoring", e);
            }
        }
    }

    public static String capitalizeFirstLetter(String str) {
        if (str == null || str.length() == 0) {
            return str;
        }
        str = str.toLowerCase();
        StringBuilder sb = new StringBuilder(str);
        sb.setCharAt(0, Character.toUpperCase(sb.charAt(0)));
        return sb.toString();
    }
}
