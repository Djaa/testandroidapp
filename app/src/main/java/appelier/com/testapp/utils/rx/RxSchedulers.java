package appelier.com.testapp.utils.rx;

import rx.Scheduler;

/**
 * Created by djaa on 07.12.17.
 */

public interface RxSchedulers {
    Scheduler runOnBackground();

    Scheduler io();

    Scheduler compute();

    Scheduler androidThread();

    Scheduler internet();
}
